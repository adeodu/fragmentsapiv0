FROM mhart/alpine-node:6.2.0

RUN mkdir -p /usr/src/app  
WORKDIR /usr/src/app  
COPY . /usr/src/app

EXPOSE 8085  
# RUN npm install -g redis-cli
RUN npm install

CMD node scripts/web-server.js

# You can then build and run the Docker image:
# $ docker build -t fragments-api-v0 .
# $ docker run -it -p 85:8085 fragments-api-v0
