module.exports = function(app, port, apiRoutes) {
	console.log('just entered FragmentsApi');
	// apply the routes to our application with the prefix /fragment
	// app.use('/fragment', apiRoutes);

	// Load needed fragments for page insertion
	apiRoutes.get('/footer', function(req, res) {
		console.log('Getting fragment for path /msportal/fragment/footer, req is: ');
		console.log(req.body);
		res.writeHead(200, {'Content-Type': 'text/html'});
   		res.write('<div style="position:absolute;width:98%;bottom:0px;display: flex;color:white;background-color:black;padding-top:5px;padding-bottom:5px;padding-left:20px;"><h1 style="width:60%;"><strong>mSportal </strong><em>Help</em></h1><div style="padding-top:30px;font-size:20px;margin-right:20px;">Services Catalog</div><div style="padding-top:30px;font-size:20px;margin-right:20px;">Guides</div><div style="padding-top:30px;font-size:20px;margin-right:20px;"><a href="http://192.168.99.100/msportal/home/" style="text-decoration:none;color:white;">Portal Home</a></div></div>');
		res.end();
		res.send();
	});

	apiRoutes.get('/header', function(req, res) {
		console.log('Getting fragment for path /msportal/fragment/header, req is: ');
		res.sendFile(__dirname + '/fragments/header.html');
	});

	apiRoutes.get('/header_css', function(req, res) {
		console.log('Getting fragment for path /msportal/fragment/header_css, req is: ');
		res.sendFile(__dirname + '/css/header.css');		
	});

	apiRoutes.get('/cartheader', function(req, res) {
		console.log('Getting fragment for path /msportal/fragment/cartheader, req is: ');
		res.sendFile(__dirname + '/fragments/cartheader.html');
	});

	apiRoutes.get('/cartheader_css', function(req, res) {
		console.log('Getting fragment for path /msportal/fragment/cartheader_css, req is: ');
		res.sendFile(__dirname + '/css/cartheader.css');		
	});

};