// Author: Adetunji Oduyela
// Date: Jul,22, 2017

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var es = require('event-stream');
var watch = require('gulp-watch');
var minifycss = require('gulp-minify-css');
var minifyhtml = require('gulp-htmlmin');
var minify = require('gulp-minify');
var rename = require('gulp-rename');
var server = require('gulp-develop-server');
var gzip = require('gulp-gzip');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var del = require('del');
var templateCache = require('gulp-angular-templatecache');

// gulp.task('clean:dist', function () {
//   return del([
//       'app/dist/**/*'
//   ]);
// });

gulp.task('templates', function () {
  return gulp.src('app/templates/**/*.html')
    .pipe(templateCache())
    .pipe(gulp.dest('app/dist'));
});

gulp.task('lint', function() {
    gulp.src([
    'app/js/*.js'
  ])
    .pipe(jshint())
    .pipe(jshint.reporter(stylish))
});

gulp.task('watch', function (event) {
  console.log('watch triggered...');
  console.log(event);
  gulp.watch('app/js/**/*.js', ['appscripts']);
});

gulp.task('appscripts', function () { 
  gulp.src([
    'app/js/*.js',
    'app/js/templates.js'
  ])
  .pipe(concat('fragmentsv0.js'))
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest('app/dist'))
});

gulp.task('compress', function() {
    gulp.src('app/dist/*')
  .pipe(gzip())
  .pipe(gulp.dest('app/dist/gzip'));
});

gulp.task( 'server:start', function() {
    server.listen( { path: 'scripts/web-server.js' } );
});
 
gulp.task( 'server:restart', function() {
    gulp.watch( [ 'app/js/app.js' ], server.restart );
});

// default task
gulp.task('default', ['templates', 'scripts', 'watch'], function () {
  // just run gulp at the console to have this started and all files watched continously
});

gulp.task('min', ['templates', 'appscripts', 'watch'], function () {
  // just run gulp at the console to have this started and all files watched continously
});

gulp.task('serve', ['templates', 'appscripts', 'compress', 'server:start', 'watch'], function () {
  // just run gulp at the console to have this started and all files watched continously
});

// //Webservice and live reload
// gulp.task('webserver', function () {
//     connect.server({
//         root: ['dist'],
//         port: 8065,
//         livereload: {
//             port: 8081
//         }
//     });
// });
