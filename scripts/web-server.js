var express = require('express');
var path = require('path');
var app = express();

var bodyParser = require('body-parser');

// Configuration
// var rootPath = path.normalize(__dirname + '/../');
// var port = 8085;
var port = process.env.PORT || 8085;
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(express.static(__dirname + '/'));

// get an instance of the router for API routes
var apiRoutes = express.Router();
app.use(express.Router());

app.use('/fragment', apiRoutes);

// Common API Routes
var fragmentsApi = require('../app/js/fragmentsApi')(app, port, apiRoutes);

app.use('*', function(req, res) {
	res.writeHead(200, {'Content-Type': 'text/html'});
	res.write('<div style="margin-left:20;margin-top:30px;"><h2>Fragment Not Found!</h2></div></div>');
	res.end();
	res.send();
});

app.listen(port, '', function() {
	console.log('Express started on ' + port + ';. press Ctrl-C to terminate.');
});
